function animar(robot){
    robot.src = "imagenes/robot.gif";
}

function desanimar(robot){
    robot.src = "imagenes/logo.png";
    robot.style.width = "200px";
    robot.style.height = "150px";
}

function falta1(verif1){
    if(verif1.value.length==0){
        document.getElementById("falta01").innerHTML = "* falta este campo";
        document.getElementById("falta01").style.color = "red";
        document.getElementById("falta01").style.fontSize = "small";
    }else{
        document.getElementById("falta01").innerHTML = "";
    }
}

function falta2(verif2){
    if(verif2.value.length==0){
        document.getElementById("falta02").innerHTML = "* falta este campo";
        document.getElementById("falta02").style.color = "red";
        document.getElementById("falta02").style.fontSize = "small";
        document.getElementById("inicnol").innerHTML = "";
        document.getElementById("termina").innerHTML = "";
    }else{
        document.getElementById("falta02").innerHTML = "";

        //verif si el email empieza con letras o numeros
        var numeros = "1234567890";
        var letras = "abcdefghijklmnñopqrstuvxyz";
        var verinum = 0;
        var verilet = 0;
        var c;
        
        for(c=0;c<numeros.length;c++){
            if(verif2.value.charAt(0)==numeros.charAt(c)){
              verinum = 1;
            }
        }
  
        for(c=0;c<letras.length;c++){
            if(verif2.value.charAt(0)==letras.charAt(c)){
              verilet = 1;
            }
        }
  
        if((verinum==0)&&(verilet==0)){
            document.getElementById("inicnol").innerHTML = "* el email no empieza con letra o número";
            document.getElementById("inicnol").style.color = "red";
            document.getElementById("inicnol").style.fontSize = "small";
        }else{
            document.getElementById("inicnol").innerHTML = "";
        }

        //verif si el email termina con .com .net .mx o .org
        var dotcom = verif2.value.indexOf('.com');
        var dotnet = verif2.value.indexOf('.net');
        var dotmx = verif2.value.indexOf('.mx');
        var dotorg = verif2.value.indexOf('.org');

        if((dotcom==-1)&&(dotnet==-1)&&(dotmx==-1)&&(dotorg==-1)){
            document.getElementById("termina").innerHTML = "* el email no termina con .com .net .mx o .org";
            document.getElementById("termina").style.color = "red";
            document.getElementById("termina").style.fontSize = "small";
        }else{
            document.getElementById("termina").innerHTML = "";
        }
    }
}

function falta3(verif3){
    if(verif3.value.length==0){        
        document.getElementById("falta03").innerHTML = "* falta este campo";
        document.getElementById("falta03").style.color = "red";
        document.getElementById("falta03").style.fontSize = "small";
        document.getElementById("dieznum").innerHTML = "";
    }else{
        document.getElementById("falta03").innerHTML = "";

        //verif si el num tiene 10 digitos

        if(verif3.value.length!=10){
            document.getElementById("dieznum").innerHTML = "* el número no es de 10 dígitos";
            document.getElementById("dieznum").style.color = "red";
            document.getElementById("dieznum").style.fontSize = "small";
        }else{
            document.getElementById("dieznum").innerHTML = "";
        }
    }
}

function falta4(verif4){
    if(verif4.value.length==0){
        document.getElementById("falta04").innerHTML = "* falta este campo";
        document.getElementById("falta04").style.color = "red";
        document.getElementById("falta04").style.fontSize = "small";
    }else{
        document.getElementById("falta04").innerHTML = "";
    }
}

function validarFrecuencia(verif5){
    if(verif5.value=="sinseleccionar"){
        document.getElementById("frec").innerHTML = "* no ha seleccionado una opción";
        document.getElementById("frec").style.color = "red";
        document.getElementById("frec").style.fontSize = "small";
    }else{
        document.getElementById("frec").innerHTML = "";
    }
}

function validarTodos(form){
    var advertencia1 = document.getElementsByName("Nombre")[0].value;
    var advertencia2 = document.getElementsByName("correo")[0].value;
    var advertencia21 = document.getElementById("inicnol").innerHTML;
    var advertencia22 = document.getElementById("termina").innerHTML;
    var advertencia3 = document.getElementsByName("tel")[0].value;
    var advertencia31 = document.getElementById("dieznum").innerHTML;
    var advertencia4 = document.getElementsByName("descripcion")[0].value;
    var advertencia5 = document.getElementsByName("frecuencia")[0].value;
    var advertencia51 = document.getElementById("frec").innerHTML;

    if((advertencia1.length==0)||(advertencia2.length==0)||(advertencia21.length>0)||(advertencia22.length>0)||(advertencia3.length==0)||(advertencia31.length>0)||(advertencia4.length==0)||(advertencia5=="sinseleccionar")||(advertencia51.length>0)){
        window.alert('Campos Incompletos\nVolver a ingresar datos');
        return false;
    }else{
        if( window.confirm('¿Desea salir del sitio?'))
        {          
            form.action = 'suscripcion.php';
            return true;
        }
        else
        {
            window.alert('No hay problema');
            return false;
        }
    }
}