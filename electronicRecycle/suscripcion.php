<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>Registro Completado</title>
		<style type="text/css">
			body {margin: 75px; 
			background-color: #222930;
			font-family: Verdana, Helvetica, sans-serif;
			font-size: 90%;}

			h1 {text-align: center;
                    font: bold 2em Georgia, serif;
                    text-shadow: .3em .3em .5em gray;
                    color: #483D8B;}
			h2 {font-size: 1.2em;
			color: #4A0048;}
			#informacion{
				background-color:  rgb(88, 191, 137);
                    line-height: 1.5em;
                    padding: 1em;
                    border-color: rgb(3, 128, 80);
                    border-style:double;
                    border-radius: 30px;
			}
			p{
				font-family: "Lucida Console", "Liberation Mono", "Courier New",Courier, monospace;
			}
			/*li strong{
				font-style: oblique;
			}*/
			h2{
				text-decoration: underline; 
				font-style: oblique;
			}
		</style>
	</head>
	<body>
		
<div id="informacion">
<h1>¡MUCHAS GRACIAS!</h1>
		<p>Gracias por llenar el formulario de suscripcion. Hemos recibido la siguiente información de tu registro:</p>

		<h2>Acerca de ti:</h2>
		<ul>
			<li><strong>Nombre:</strong> <em><?php echo $_POST['Nombre']; ?></em></li>
			<li><strong>E-mail:</strong> <em><?php echo $_POST['correo']; ?></em></li>
			<li><strong>Télefono:</strong> <em><?php echo $_POST['tel']; ?></em></li>
		</ul>
		<p><strong>La razón de tu suscripción:</strong> <em><?php echo $_POST['descripcion']; ?></em></p>

		<h2>Medios de contacto</h2>
		<ul>
			<li><strong>Medio:</strong> <em><?php echo $_POST['medio']; ?></em></li>
		</ul>
        <h2>Frecuencia de contacto</h2>
        <ul>
            <li><strong>Frecuencia:</strong> <em><?php echo $_POST['frecuencia']; ?></em></li>
		</ul>
</div>
	</body>
</html>